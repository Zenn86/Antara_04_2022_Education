package animals.types;

import animals.AbstractAnimal;
import food.AbstractFood;
import food.types.AbstractMeat;

public abstract class AbstractHerbivore extends AbstractAnimal {

    public AbstractHerbivore() {
        super();
    }

    public AbstractHerbivore(String name, int weight) {
        super(name, weight);
    }

    @Override
    public void eat(AbstractFood food) {
        if (food instanceof AbstractMeat) {
            System.out.println("This animal is a Herbivore. Give him some grass!");
            return;
        }
        setSatiety(food.getEnergy() / 100);
        setWeight(food.getWeight());
        System.out.println(getName() + " has eaten a " + food.getName() + getStatus());
    }

}
