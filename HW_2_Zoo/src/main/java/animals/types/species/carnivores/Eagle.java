package animals.types.species.carnivores;

import animals.interfaces.Fly;
import animals.interfaces.Voice;
import animals.types.AbstractCarnivore;

public class Eagle extends AbstractCarnivore implements Fly, Voice {

    private final String voice = "Eagle sound!";

    public Eagle() {
        super();
        setSpecie("The Eagle");
        setName(getName() + " " + getSpecie());
    }

    public Eagle(String name, int weight ) {
        super(name, weight);
        setSpecie("The Eagle");
        setName(getName() + " " + getSpecie());
    }

    @Override
    public void fly() {
        setSatiety(-2);
        setWeight(-1);
        System.out.print(getSpecie() + fly);
        System.out.println(getStatus());
    }

    @Override
    public String voice() {
        return voice;
    }
}
