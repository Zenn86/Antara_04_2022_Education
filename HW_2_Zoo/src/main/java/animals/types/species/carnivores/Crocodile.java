package animals.types.species.carnivores;

import animals.interfaces.Swim;
import animals.types.AbstractCarnivore;

public class Crocodile extends AbstractCarnivore implements Swim {

    public Crocodile() {
        super();
        setSpecie("The Crocodile");
        setName(getName() + " " + getSpecie());
    }

    public Crocodile(String name, int weight ) {
        super(name, weight);
        setSpecie("The Crocodile");
        setName(getName() + " " + getSpecie());
    }

    @Override
    public void swim() {
        setSatiety(-2);
        setWeight(-1);
        System.out.print(getSpecie() + swim);
        System.out.println(getStatus());
    }
}
