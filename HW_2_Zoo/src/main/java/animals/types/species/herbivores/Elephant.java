package animals.types.species.herbivores;

import animals.interfaces.Run;
import animals.interfaces.Swim;
import animals.interfaces.Voice;
import animals.types.AbstractHerbivore;

public class Elephant extends AbstractHerbivore implements Run, Swim, Voice {

    private final String voice = "Elephant sound!";

    public Elephant() {
        super();
        setSpecie("The Elephant");
        setName(getName() + " " + getSpecie());
    }

    public Elephant(String name, int weight ) {
        super(name, weight);
        setSpecie("The Elephant");
        setName(getName() + " " + getSpecie());
    }

    @Override
    public void run() {
        setSatiety(-4);
        setWeight(-5);
        System.out.print(getSpecie() + run);
        System.out.println(getStatus());
    }

    @Override
    public void swim() {
        setSatiety(-3);
        setWeight(-3);
        System.out.print(getSpecie() + swim);
        System.out.println(getStatus());
    }

    @Override
    public String voice() {
        return voice;
    }
}
