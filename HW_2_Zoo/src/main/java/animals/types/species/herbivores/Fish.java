package animals.types.species.herbivores;

import animals.interfaces.Swim;
import animals.types.AbstractHerbivore;

public class Fish extends AbstractHerbivore implements Swim {

    public Fish() {
        super();
        setSpecie("The Fish");
        setName(getName() + " " + getSpecie());
    }

    public Fish(String name, int weight ) {
        super(name, weight);
        setSpecie("The Fish");
        setName(getName() + " " + getSpecie());
    }

    @Override
    public void swim() {
        setSatiety(-1);
        setWeight(-1);
        System.out.print(getSpecie() + swim + getStatus());
        System.out.println(getStatus());
    }
}
