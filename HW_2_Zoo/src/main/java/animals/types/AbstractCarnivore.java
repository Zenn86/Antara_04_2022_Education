package animals.types;

import animals.AbstractAnimal;
import food.AbstractFood;
import food.types.AbstractGreen;

public abstract class AbstractCarnivore extends AbstractAnimal {

    public AbstractCarnivore() {
        super();
    }

    public AbstractCarnivore(String name, int weight) {
        super(name, weight);
    }

    @Override
    public void eat(AbstractFood food) {
        if (food instanceof AbstractGreen) {
            System.out.println("This animal is a Carnivore. Give him something meaty!");
            return;
        }
        setSatiety(food.getEnergy() / 100);
        setWeight(food.getWeight());
        System.out.println(getName() + getStatus());
    }

}
