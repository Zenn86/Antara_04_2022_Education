package animals;

import food.AbstractFood;

public abstract class AbstractAnimal {

    private int satiety;
    private int weight;
    private String name;
    private String specie;
    private String status = ". Current satiety: " + getSatiety() + ". Current weight: " + getWeight() + ".";

    public AbstractAnimal() {
        setName("John Doe");
        setWeight(10 + (int) (Math.random() * 300));
        setSatiety(10);
    }

    public AbstractAnimal(String name, int weight) {
        if (weight <= 0) {
            throw new IllegalArgumentException();
        }
        setName(name);
        setWeight(weight);
        setSatiety(10);
    }

    public abstract void eat(AbstractFood food);

    public int getSatiety() {
        return satiety;
    }

    public int getWeight() {
        return weight;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public String getSpecie() {
        return specie;
    }

    public void setSatiety(int inputSatiety) {
        this.satiety += inputSatiety;
        setStatus();
    }

    public void setWeight(int inputWeight) {
        this.weight += inputWeight;
        setStatus();
    }

    public void setName(String name) {
        this.name = name;
    }

    private void setStatus() {
        this.status = ". Current satiety: " + getSatiety() + ". Current weight: " + getWeight() + ".";
    }

    public void setSpecie(final String specie) {
        this.specie = specie;
    }
}
