package worker;

import animals.AbstractAnimal;
import animals.interfaces.Voice;
import food.AbstractFood;

public class Worker {

    public void feed(AbstractAnimal animal, AbstractFood food) {
        System.out.println("The Worker tried to feed " + animal.getName());
        animal.eat(food);
    }

    public void getVoice(Voice animal) {
        System.out.println("The worker forced the animal to speak.");
        System.out.println(animal.voice());
    }
}
