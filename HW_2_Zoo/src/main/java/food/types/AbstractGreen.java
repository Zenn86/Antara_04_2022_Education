package food.types;

import food.AbstractFood;

public abstract class AbstractGreen extends AbstractFood {

    public AbstractGreen() {
        super();
    }

    public AbstractGreen(int weight) {
        super(weight);
    }
}
