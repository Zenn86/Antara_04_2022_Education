package food.types;

import food.AbstractFood;

public abstract class AbstractMeat extends AbstractFood {

    public AbstractMeat() {
        super();
    }

    public AbstractMeat(int weight) {
        super(weight);
    }

}
