package food.types.products.meat;

import food.types.AbstractMeat;

public class Beef extends AbstractMeat {

    public Beef() {
        super();
        setEnergy(getWeight() * 300);
        setName("Beef");
    }

    public Beef(int weight) {
        super(weight);
        setEnergy(getWeight() * 300);
        setName("Beef");
    }
}
