package food.types.products.meat;

import food.types.AbstractMeat;

public class Pork extends AbstractMeat {

    public Pork() {
        super();
        setEnergy(getWeight() * 250);
        setName("Pork");
    }

    public Pork(int weight) {
        super(weight);
        setEnergy(getWeight() * 250);
        setName("Pork");
    }

}
