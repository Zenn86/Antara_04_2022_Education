package food.types.products.meat;

import food.types.AbstractMeat;

public class Chicken extends AbstractMeat {

    public Chicken() {
        super();
        setEnergy(getWeight() * 200);
        setName("Chicken");
    }

    public Chicken(int weight) {
        super(weight);
        setEnergy(getWeight() * 200);
        setName("Chicken");
    }

}
