package food.types.products.green;

import food.types.AbstractGreen;

public class Apple extends AbstractGreen {

    public Apple() {
        super();
        setEnergy(getWeight() * 150);
        setName("Apple");
    }

    public Apple(int weight) {
        super(weight);
        setEnergy(getWeight() * 150);
        setName("Apple");
    }
}
