package food.types.products.green;

import food.types.AbstractGreen;

public class Carrot extends AbstractGreen {

    public Carrot() {
        super();
        setEnergy(getWeight() * 100);
        setName("Carrot");
    }

    public Carrot(int weight) {
        super(weight);
        setEnergy(getWeight() * 100);
        setName("Carrot");
    }
}
