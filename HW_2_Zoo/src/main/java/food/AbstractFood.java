package food;

public abstract class AbstractFood {
    private int weight;
    private int energy;
    private String name;

    public AbstractFood() {
        this.weight = (1 + (int) (Math.random() * 10));
    }

    public AbstractFood(int weight) {
        this.weight = weight;
    }

    public int getEnergy() {
        return energy;
    }

    public int getWeight() {
        return weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }
}
