package model;

public class Kotik {
    private static int quantityOfCats = 0;
    private int satiety = 0;
    private int weight;
    private int prettiness;
    private String name;
    private String meow;

    public Kotik() {
        quantityOfCats += 1;
    }

    public Kotik(int inputPrettiness, String inputName, int inputWeight, String inputMeow) {
        quantityOfCats += 1;
        this.setKotik(inputPrettiness, inputName, inputWeight, inputMeow);
    }

    public boolean eat() {
        return this.eat(2, "fish");
    }

    public boolean eat(int inputSatiety) {
        satiety += inputSatiety;
        System.out.println("The cat has eaten.");
        return true;
    }

    public boolean eat(int inputSatiety, String food) {
        satiety += inputSatiety;
        System.out.println("The cat has eaten a " + food);
        return true;
    }

    public boolean play() {
        if (satiety <= 0) {
            System.out.print("The cat hasn't played. Feed the cat! ");
            return false;
        } else {
            System.out.println("The cat has played!");
            return true;
        }
    }

    public boolean sleep() {
        if (satiety <= 0) {
            System.out.print("The cat hasn't slept. Feed the cat! ");
            return false;
        } else {
            System.out.println("The cat has slept!");
            return true;
        }
    }

    public boolean chaseMouse() {
        if (satiety <= 0) {
            System.out.print("The cat hasn't chased mouse. Feed the cat! ");
            return false;
        } else {
            System.out.println("The cat has catch the mouse!");
            return true;
        }
    }

    public boolean purr() {
        if (satiety <= 0) {
            System.out.print("The cat hasn't purred. Feed the cat! ");
            return false;
        } else {
            System.out.println("The cat has purred!");
            return true;
        }
    }

    public void liveAnotherDay() {
        for (int i = 0; i < 24; i++) {
            switch ((int) (Math.random() * 5 + 1)) {
                case 1:
                    eat();
                    break;
                case 2:
                    if (play()) {
                        break;
                    } else {
                        eat();
                        break;
                    }
                case 3:
                    if (sleep()) {
                        break;
                    } else {
                        eat();
                        break;
                    }
                case 4:
                    if (chaseMouse()) {
                        break;
                    } else {
                        eat();
                        break;
                    }
                case 5:
                    if (purr()) {
                        break;
                    } else {
                        eat();
                        break;
                    }
                default:
                    throw new IllegalStateException("Unexpected value: " + (int) (Math.random() * 5 + 1));
            }
        }
    }

    public void setKotik(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
    }

    public static int getQuantityOfCats() {
        return quantityOfCats;
    }

    public int getSatiety() {
        return satiety;
    }

    public int getWeight() {
        return weight;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public String getName() {
        return name;
    }

    public String getMeow() {
        return meow;
    }
}
